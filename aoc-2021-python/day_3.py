input_fn = "aoc-2021-python/day_3_input.txt"

def main():
    with open(input_fn) as f:
        out = [l[:-1] for l in f.readlines()]

    max_count = len(out)
    counts = [0 for i in range(len(out[0]))]
    for r in out:
        for i, c in enumerate(r):
            counts[i] += int(c)
    gamma = [c>(max_count/2) for c in counts]
    gamma_val = int("".join(["0","1"][b] for b in gamma), base=2)
    eps_val = int("".join(["1","0"][b] for b in gamma), base=2)
    print(gamma_val*eps_val)

if __name__ == "__main__":
    main()
