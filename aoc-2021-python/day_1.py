input_fn = "aoc-2021-python/day_1_input.txt"


def solve_1():
    with open(input_fn) as f:
        prev = None
        inc_count = 0
        for l in f.readlines():
            num = int(l)
            if prev:
                inc_count += num > prev
            prev = num
    print(inc_count)


def solve_2():
    with open(input_fn) as f:
        out = [int(l) for l in f.readlines()]

    inc_count = 0
    prev_sum = None    
    for depths in zip(out, out[1:], out[2:]):
        s = sum(depths)
        if prev_sum:
            inc_count += s > prev_sum
        prev_sum = s

    print(inc_count)


if __name__ == "__main__":
    solve_1()
    solve_2()
