import re


input_fn = "aoc-2021-python/day_2_input.txt"


def solve_1():
    with open(input_fn) as f:
        dir_val = [re.findall(r"(.*) (.)", l)[0] for l in f.readlines()]
    x, y = 0, 0
    for d, val in dir_val:
        val = int(val)
        if d == "up":
            y -= val
        elif d == "down":
            y += val
        elif d == "forward":
            x += val
        else:
            x -= val
    print(x*y)


def solve_2():
    with open(input_fn) as f:
        dir_val = [re.findall(r"(.*) (.)", l)[0] for l in f.readlines()]
    x, aim, y = 0, 0, 0
    for d, val in dir_val:
        val = int(val)
        if d == "up":
            aim -= val
        elif d == "down":
            aim += val
        else:
            x += val
            y += aim * val
    print(x*y)


if __name__ == "__main__":
    solve_1()
    solve_2()
