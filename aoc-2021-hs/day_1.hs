
higher_int :: Int -> Int -> Int
higher_int a b =  fromEnum (a>b)

solve_1 :: [Char] -> Int
solve_1 a = (
   int_values = [read x :: Int | x <-(lines a)]
   int_values_1 = drop 1 int_values
   diffs = zipWith (higher_int) int_values_1 int_values
   s = foldl (+) 0 diffs
   return s
)

main = do
   content <- readFile ("day_1_input.txt")
   solve(content)
   let linesOfFiles = lines content
   let int_values = [read x :: Int | x <-linesOfFiles]
   let int_values_1 = drop 1 int_values
   let diffs = zipWith (higher_int) int_values_1 int_values
   let s = foldl (+) 0 diffs
   print s