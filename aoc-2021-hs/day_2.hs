
higher_int :: Int -> Int -> Int
higher_int a b =  fromEnum (a>b)

main = do
   content <- readFile ("day_1_input.txt")
   let linesOfFiles = lines content
   let int_values = [read x :: Int | x <-linesOfFiles]
   let int_values_1 = drop 1 int_values
   let int_values_2 = drop 2 int_values
   let sums = zipWith (+) int_values int_values_1
   let sums2 = zipWith (+) sums int_values_2
   let sums2_1 = drop 1 sums2
   let diffs = zipWith (higher_int) sums2_1 sums2 
   let s = foldl (+) 0 diffs
   print s
